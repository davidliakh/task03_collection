package epam.BinaryTree;

import java.util.ArrayList;
import java.util.List;

public class MyBinaryTreeMap<V> {

    class Node {

        int key;
        V value;
        Node left;
        Node right;

        Node(int key, V value) {
            this.key = key;
            this.value = value;
            left = null;
            right = null;
        }
    }

    Node root;
    int size = 0;

    MyBinaryTreeMap() {
    }

    public void put(int key, V value) {
        Node newNode = new Node(key, value);
        if (root == null) {
            root = newNode;
            size++;
        } else {
            Node tempNode = root; //temp Node to navigate
            Node parentNode; //to navigate right or left
            while (true) {
                parentNode = tempNode; //Starting from root
                if (key == tempNode.key) { //Re-write by key new value
                    tempNode.value = value;
                    return;
                } else if (key < tempNode.key) { //If less go left
                    tempNode = tempNode.left;
                    if (tempNode == null) {
                        parentNode.left = newNode;
                        size++;
                        return;
                    }
                } else { //If more than go right
                    tempNode = tempNode.right;
                    if (tempNode == null) {
                        parentNode.right = newNode;
                        size++;
                        return;
                    }
                }
            }

        }
    }

    public V get(int key) {
        Node node = find(key);
        if (node != null) {
            return node.value;
        } else {
            return null;
        }
    }

    public int size() {
        return size;
    }

    private Node find(int key) {
        Node temp = root;
        if (root == null) {
            return null;
        } else if (temp.key == key) {
            return temp;
        } else {
            while (true) {
                if (key < temp.key) {
                    if (temp.left != null) {
                        temp = temp.left;
                        if (key == temp.key) {
                            return temp;
                        }
                    } else {
                        return null;
                    }
                } else {
                    if (temp.right != null) {
                        temp = temp.right;
                        if (key == temp.key) {
                            return temp;
                        }
                    } else {
                        return null;
                    }
                }
            }
        }
    }

    public void remove(final int key) {
        List<Node> list = new ArrayList<>();
        preorderNode(root, list);
        root = null;
        list.stream().filter(i -> i.key != key).forEach(i -> put(i.key, i.value));
    }

    public void print() {
        print("", root, true);
    }

    private void print(String prefix, Node n, boolean isLeft) {
        if (n != null) {
            System.out.println(prefix + (isLeft ? "|-- " : "\\-- ") + n.key);
            print(prefix + (isLeft ? "|   " : "    "), n.left, true);
            print(prefix + (isLeft ? "|   " : "    "), n.right, false);
        }
    }

    private void preorderNode(Node node, List<Node> arrayList) {
        if (node == null) {
            return;
        } else {
            arrayList.add(node);
            preorderNode(node.right, arrayList);
            preorderNode(node.left, arrayList);
        }
    }
}
