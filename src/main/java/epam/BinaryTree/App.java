package epam.BinaryTree;

import java.util.Scanner;

public class App {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int input;
        MyBinaryTreeMap<String> treeMap = new MyBinaryTreeMap();
        do {
            System.out.println(Menu.ONE.getId() + ": to PUT something in tree");
            System.out.println(Menu.TWO.getId() + ": to GET something from tree");
            System.out.println(Menu.THREE.getId() + ": to REMOVE something from tree");
            System.out.println(Menu.FOUR.getId() + ": to PRINT your tree");
            System.out.println(Menu.SIZE.getId() + ": to show SIZE of your tree");
            System.out.println(Menu.EXIT.getId() + ": to EXIT program.");
            do {
                System.out.println("Enter menu choice: ");
                while (!scanner.hasNextInt()) {
                    scanner.next();
                }
                input = scanner.nextInt();
            }
            while (Menu.getMenu(input) == null);
            switch (Menu.getMenu(input)) {
                case ONE:
                    int key;
                    String value;
                    System.out.println("What to put? KEY: ");
                    while (!scanner.hasNextInt()) {
                        scanner.next();
                    }
                    key = scanner.nextInt();
                    System.out.println("What to put? VALUE: ");
                    value = scanner.next();
                    treeMap.put(key, value);
                    System.out.println("Putted value: " + value + " by key: " + key);
                    break;
                case TWO:
                    System.out.println("What to get? Enter key: ");
                    while (!scanner.hasNextInt()) {
                        scanner.next();
                    }
                    key = scanner.nextInt();
                    System.out.println("KEY: " + key + " VALUE: " + treeMap.get(key));
                    break;
                case EXIT:
                    System.exit(0);
                    break;
                case THREE:
                    System.out.println("Enter key to remove node: ");
                    while (!scanner.hasNextInt()) {
                        scanner.next();
                    }
                    key = scanner.nextInt();
                    treeMap.remove(key);
                    break;
                case FOUR:
                    treeMap.print();
                    break;
                case SIZE:
                    System.out.println("Size of this tree: " + treeMap.size());
                    break;
                default:
                    System.out.println("You entered wrong number!");
                    break;
            }
        } while (input != Menu.EXIT.getId());
    }
}

