package epam.PriorityQueue;

import java.util.ArrayList;

public class MyPriorityQueue<V> {
    ArrayList<V> arrayList = new ArrayList<V>();

    public void insert(V value) {
        arrayList.add(value);
    }

    public void printQueue() {
        for (int i = 0; i < arrayList.size(); i++) {
            System.out.println(arrayList.get(i));
        }
    }

    public void remove(V value) {
        if (arrayList.isEmpty()) {
            System.out.println("Queue is empty");
        }
        arrayList.remove(value);
    }

    public void getIndex(V value) {
        System.out.println(arrayList.indexOf(value));
    }

    public void printIndexesOfQueue() {
        for (int i = arrayList.size(); i > 0; i--) {
            System.out.println(i);
        }
    }

    public void printValueByIndex(int index) {
        System.out.println(arrayList.get(index));
    }
}
