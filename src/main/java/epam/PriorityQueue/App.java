package epam.PriorityQueue;

public class App {
    public static void main(String[] args) {
        MyPriorityQueue a = new MyPriorityQueue();
        a.insert("String");
        a.insert(1234);
        a.insert("xxxx1234ddd");
        a.insert(14.156);
        a.insert('b');
        a.insert(0.45);
        a.printQueue();
        System.out.println("------------------------");
        a.remove(14.156);
        a.printQueue();
        System.out.println("------------------------");
        a.getIndex('b');
        System.out.println("------------------------");
        a.printIndexesOfQueue();
        System.out.println("------------------------");
        a.printValueByIndex(4);
    }
}
