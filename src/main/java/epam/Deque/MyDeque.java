package epam.Deque;

import java.util.LinkedList;

public class MyDeque<V> {
    LinkedList<V> deque = new LinkedList<V>();

    public void addFirst(V value) {
        deque.addFirst(value);
    }

    public void addLast(V value) {
        deque.addLast(value);
    }

    public void getFirst() {
        System.out.println(deque.getFirst());
    }

    public void getLast() {
        System.out.println(deque.getLast());
    }

    public void printDeque() {
        for (int i = 0; i < deque.size(); i++) {
            System.out.println(deque.get(i));
        }
    }

    public V removeFirst() {
        return deque.removeFirst();
    }

    public V removeLast() {
        return deque.removeLast();
    }

}
