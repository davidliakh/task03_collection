package epam.Deque;

public class App {
    public static void main(String[] args) {
        MyDeque a = new MyDeque();
        a.addFirst("abcde");
        a.addFirst(123456);
        a.addFirst('a');
        a.addFirst(13.567);
        a.addFirst("0.5");
        a.addLast(2344);
        a.addLast("er31fg");
        a.addLast(000.555);
        a.printDeque();
        System.out.println("-------------------");
        System.out.println(a.removeFirst());
        System.out.println("-------------------");
        a.printDeque();
    }
}
